package pqp;

public class Person {

	 /**
	 * @uml.property  name="name"
	 */
	private String name;
	 /**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {return name;}
	    
     public Person(String name) {
    	 this.name=name;
     }
     public String toString() {
    	 return this.name;
     }

}
