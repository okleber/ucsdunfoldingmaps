package pqp;

public class test2 {
	static public class test{
		public int value;
		
		public test(int v) {
			this.value=v;
		}
		
		public int get() {
			return this.value;
		}
	}
	
	public static void main(String[] args) {
		test a=new test(1);
		test b=new test(2);
		test c=new test(3);
		a=b;

		b=c;
		
		System.out.println(a.get());
		System.out.println(b.get());
		System.out.println(c.get());

	}
}
