package pqp;

public class Test3
{

private int a;

public double b;
  
  public Test3(int first, double second)
  {
    this.a = first;
    this.b = second;
  }
  public static void main(String[] args)
  {
    Test3 c1 = new Test3(10, 20.5);
    Test3 c2 = new Test3(10, 31.5);
    // lines below are changed from the question above
    c2 = c1;   
    c1.a = 2;
    System.out.println(c2.a);
    xx();
  }
  public static void xx() {
	  Test3 c1 = new Test3(10, 20.5);
	  System.out.println(c1.a);
  }
}