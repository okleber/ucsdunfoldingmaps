package pqp;

import java.util.ArrayList;

public class TestSorts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] vals= {99,100,1,5,6,2,65,436,35,234,6,3};
		selectionSort(vals);
		for(int i: vals) {
			System.out.println(i);
		}
	}
	
	public static void selectionSort(int[] vals) {
		int count=0;
		
		while(count < vals.length-1) {
			int indexMin=count;
			for(int i=count+1; i < vals.length;i++) {
				if(vals[i]<vals[indexMin]) {
					indexMin=i;
				}
			}
			swap(vals, indexMin,count);
			count++;
		}
	}
	
	public static void swap(int[] vals, int min, int pos) {
		int temp;
		temp=vals[pos];
		vals[pos]=vals[min];
		vals[min]=temp;
	}

}
