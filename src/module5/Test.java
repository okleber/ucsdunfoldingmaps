package module5;

import finalmodule.GetAccessToken;
import finalmodule.GetAvailableTrends;
import twitter4j.TwitterFactory;
import twitter4j.Twitter;


public class Test {

	public static void main(String[] args) {
	    Twitter twitter = new TwitterFactory().getInstance();
	    GetAccessToken g = new GetAccessToken();
	    g.getAccessToken(twitter);
	    GetAvailableTrends t =new GetAvailableTrends();
	    double lo=(double)-43.17289;
	    double la=(double)-22.90684;
	    t.getTrends(twitter,la,lo);
	}

}
