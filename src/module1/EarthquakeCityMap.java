package module1;

import java.util.ArrayList;
import java.util.List;
import processing.core.PApplet;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.providers.Google;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;
import de.fhpotsdam.unfolding.utils.MapUtils;
import parsing.ParseFeed;

public class EarthquakeCityMap extends PApplet {


	/**
	 * @uml.property  name="map"
	 * @uml.associationEnd  
	 */
	private UnfoldingMap map;

	
	public void setup() {
		size(1024, 768, OPENGL);
		map = new UnfoldingMap(this, 100, 0, 700, 500, new Google.GoogleMapProvider());
	    map.zoomToLevel(2);
	    MapUtils.createDefaultEventDispatcher(this, map);	
	}	
	
	public void draw() {
	    background(10);
	    map.draw();
	    addKey();
	}

	private void addKey() 
	{	
		// Remember you can use Processing's graphics methods here
	
	}
}

