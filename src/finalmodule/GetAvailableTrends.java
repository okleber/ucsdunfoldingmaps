package finalmodule;


import twitter4j.GeoLocation;
import twitter4j.Location;
import twitter4j.ResponseList;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import java.io.IOException;



public class GetAvailableTrends {
    
	
	private int getWoeid(Twitter twitter, double Latitude, double Longitude) throws TwitterException {
		int woeid=0;
		GeoLocation geo = new GeoLocation(Latitude,Longitude);
		ResponseList<Location> r = twitter.getClosestTrends(geo);
		for(Location l: r) {
			woeid=l.getWoeid();
			break;
		}
		return woeid;
	}
	
	
	public Trends getTrends(Twitter twitter, double lat, double longt){
		Trends t=null;
		try {
			int woeid = getWoeid(twitter, lat, longt);
			System.out.println("WOEID = "+woeid);
			//int woeid = 455825; This oeid works for debug purpose
			t = twitter.getPlaceTrends(woeid);
        
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to get trends: " + te.getMessage());
			System.exit(-1);
		}
		return t;
	}
}
