package demos;

/** A class to represent Airport data.
 * Used in module 6 of the UC San Diego MOOC Object Oriented Programming in Java
 * 
 * @author UC San Diego Intermediate Software Development MOOC team
 * 
 *
 */
public class Airport implements Comparable<Airport> {
	/**
	 * @uml.property  name="airportID"
	 */
	private int airportID;
	/**
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="city"
	 */
	private String city;
	/**
	 * @uml.property  name="country"
	 */
	private String country;
	/**
	 * @uml.property  name="code3"
	 */
	private String code3;
	/**
	 * @uml.property  name="code4"
	 */
	private String code4;
	/**
	 * @uml.property  name="latitude"
	 */
	private double latitude;
	/**
	 * @uml.property  name="longitude"
	 */
	private double longitude;
	/**
	 * @uml.property  name="altitude"
	 */
	private int altitude;
	/**
	 * @uml.property  name="timezone"
	 */
	private float timezone;
	/**
	 * @uml.property  name="dst"
	 */
	private char dst;
	/**
	 * @uml.property  name="dbTimezone"
	 */
	private String dbTimezone;
	
	
	public Airport(int airportID, String name, String city, String country, String code3, String code4, double latitude,
			double longitude, int altitude, float timezone, char dst, String dbTimezone) {
		this.airportID = airportID;
		this.name = name;
		this.city = city;
		this.country = country;
		this.code3 = code3;
		this.code4 = code4;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.timezone = timezone;
		this.dst = dst;
		this.dbTimezone = dbTimezone;
	}
	/**
	 * @return
	 * @uml.property  name="airportID"
	 */
	public int getAirportID() {
		return airportID;
	}
	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return
	 * @uml.property  name="city"
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @return
	 * @uml.property  name="country"
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @return
	 * @uml.property  name="code3"
	 */
	public String getCode3() {
		return code3;
	}
	/**
	 * @return
	 * @uml.property  name="code4"
	 */
	public String getCode4() {
		return code4;
	}
	/**
	 * @return
	 * @uml.property  name="latitude"
	 */
	public double getLatitude() {
		return latitude;
	}
	/**
	 * @return
	 * @uml.property  name="longitude"
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * @return
	 * @uml.property  name="altitude"
	 */
	public int getAltitude() {
		return altitude;
	}
	/**
	 * @return
	 * @uml.property  name="timezone"
	 */
	public float getTimezone() {
		return timezone;
	}
	/**
	 * @return
	 * @uml.property  name="dst"
	 */
	public char getDst() {
		return dst;
	}
	/**
	 * @return
	 * @uml.property  name="dbTimezone"
	 */
	public String getDbTimezone() {
		return dbTimezone;
	}
	
	public int compareTo(Airport other)
	{
		return this.city.compareTo(other.city);
	}
	
}
